package eStore;

import static org.junit.Assert.assertNotNull;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UsingSeleniumExampleTest extends SeleniumTest{

    @Test
    public void letMeGoogleThatForYouTest() {
        this.driver.get("https://www.google.com");
        WebElement element = this.driver.findElement(By.name("q"));
        assertNotNull(element);
        element.sendKeys("Let me google that for you\n");                
    } 
    
	@Test
	public void viewProductList() {
		this.driver.get("http://localhost:8080/eStore");
		this.driver.findElement(By.linkText("Products")).sendKeys(Keys.ENTER);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	@Test
	public void searchProductByName() {
		String productName = "king";
		this.driver.get("http://localhost:8080/eStore/product/productList/all");
		this.driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/label/input")).sendKeys(productName);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	@Test
	public void viewOrders() {
		this.driver.get("http://localhost:8080/eStore/login/");
		
		this.driver.findElement(By.id("username"));
		WebElement username = driver.findElement(By.id("username"));
		this.driver.findElement(By.id("password"));
		WebElement password = driver.findElement(By.id("password"));
		driver.findElement(By.xpath("/html/body/div[2]/div/div/form/input[1]"));
		WebElement login= driver.findElement(By.xpath("/html/body/div[2]/div/div/form/input[1]"));
		username.sendKeys("admin");
		password.sendKeys("admin");
		login.click();
		
		this.driver.findElement(By.linkText("Admin"));
		WebElement admin = driver.findElement(By.linkText("Admin"));
		admin.sendKeys(Keys.ENTER);
		
		this.driver.findElement(By.linkText("All Orders"));
		WebElement orders = driver.findElement(By.linkText("All Orders"));
		orders.sendKeys(Keys.ENTER);
		
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.driver.findElement(By.xpath("/html/body/div[1]/div/nav/div/div[2]/ul[2]/li[3]/a")).sendKeys(Keys.ENTER);
		
	}
	
	@Test
	public void manageProducts() {
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		this.driver.get("http://localhost:8080/eStore/login/");
		
		this.driver.findElement(By.id("username"));
		WebElement username = driver.findElement(By.id("username"));
		this.driver.findElement(By.id("password"));
		WebElement password = driver.findElement(By.id("password"));
		driver.findElement(By.xpath("/html/body/div[2]/div/div/form/input[1]"));
		WebElement login= driver.findElement(By.xpath("/html/body/div[2]/div/div/form/input[1]"));
		username.sendKeys("admin");
		password.sendKeys("admin");
		login.click();
		
		this.driver.findElement(By.linkText("Admin"));
		WebElement admin = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.linkText("Admin")));
//		WebElement admin = driver.findElement(By.linkText("Admin"));
		assertNotNull(admin);
		admin.sendKeys(Keys.ENTER);
		
//		this.driver.findElement(By.linkText("Product Inventory"));
		WebElement orders = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.linkText("Product Inventory")));
//		WebElement orders = driver.findElement(By.linkText("Product Inventory"));
		assertNotNull(orders);
		orders.sendKeys(Keys.ENTER);
		
//		this.driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/table/tbody/tr[1]/td[6]/a[3]/span"));
//		WebElement edit = wait.until(ExpectedConditions.elementToBeClickable(
//                By.xpath("/html/body/div[2]/div/div[2]/table/tbody/tr[1]/td[6]/a[3]/span")));
		WebElement edit = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/table/tbody/tr[1]/td[6]/a[3]/span"));
		assertNotNull(edit);
		edit.click();
		
		this.driver.findElement(By.id("name"));
		WebElement name = driver.findElement(By.id("name"));
//		WebElement name = wait.until(ExpectedConditions.visibilityOfElementLocated(
//				By.id("name")));
		name.sendKeys("Ciccio777");
		assertNotNull(name);
		WebElement submit= driver.findElement(By.xpath("/html/body/div[2]/div/form/input[2]"));
		assertNotNull(submit);
		submit.click();

		
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.driver.findElement(By.xpath("/html/body/div[1]/div/nav/div/div[2]/ul[2]/li[3]/a")).sendKeys(Keys.ENTER);


	}
}
