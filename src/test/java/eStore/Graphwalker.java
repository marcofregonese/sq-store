package eStore;

// Generated by GraphWalker (http://www.graphwalker.org)
import org.graphwalker.java.annotation.Model;
import org.graphwalker.java.annotation.Vertex;
import org.graphwalker.java.annotation.Edge;

@Model(file = "graphwalker.graphml")
public interface Graphwalker {

    @Edge()
    void e_View_all_products();

    @Edge()
    void e_Select_product();

    @Edge()
    void e_Go_to_login();

    @Edge()
    void e_Open_Browser();

    @Edge()
    void e_Go_to_eStore();

    @Vertex()
    void v_All_product_displayed();

    @Vertex()
    void e_Estore_Home_displayed();

    @Vertex()
    void v_Error_displayed();

    @Vertex()
    void v_Cart_displayed();

    @Vertex()
    void v_Browser_opened();

    @Vertex()
    void v_Login_displayed();

    @Edge()
    void e_Put_in_cart();

    @Edge()
    void e_Insert_credentials();

    @Vertex()
    void v_Product_displayed();

    @Edge()
    void e_Go_back_to_login();

    @Vertex()
    void v_Userpage_displayed();
}
